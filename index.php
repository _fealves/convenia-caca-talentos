
<!DOCTYPE html>
 <html lang="PT-BR">

    <head>
    	<meta charset="utf-8">


	    <title>Caça Talentos</title>
        <?php
            include ("includes/head.php");
        ?>
        <meta property="og:locale" content="PT-BR" />
   
        <meta property="og:title" content="Caça Talentos Convenia" />
        <meta property="og:site_name" content="Convenia" />

        <meta property="og:url" content="http://www.convenia.com.br/caca-talentos/" />
        <meta property="og:description" content="Não sabe ainda qual profissão lhe interessa? Faça um tour entre as funções do convenia. Envie seus dados e fique atento a nossa chamada." />

        <meta property="og:image" content="img/caca-talentos-fundo.png" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="440" />
        <meta property="og:type" content="website">
    
    
        
    </head>

   <body class="home">


        <?php
            include("includes/nav.php")
        ?>
            

            <div class="banner">

                <p><a href="Index.php">Home</a> > <a id="topo" href="#">Carreira</a></p>
                <hr />
                
                <div>
                    <img class="logo-banner" src="img/caca-talentos-logo-bnr.png" alt="Caça Talentos">
                </div>
                 <div id="logo-banner"></div>
                
                <p>O <b>Convenia</b> ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis.
                Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é</p>
               

            </div>

            <div class="limpar"></div>

            <section class="container-destaque">

                <div class="box box1">
                    <div class="destaque">
                        <h2 class="h1-meu-valor">QUERO PROVAR O MEU VALOR</h2>
                        <img src="img/caca-talentos-img1.png" alt="">
                        <p>Não sabe ainda qual profissão lhe interessa? Faça um "tour" entre as funções do convenia. 
                        Envie seus dados e fique atento a nossa chamada.</p>
                        <a class="bnt bnt-meu-valor" type="button" href='QueroProvarMeuValor.php'>Quero provar meu valor</a>
                    </div>
                </div>

                <div class="box box2">
                    <div class="destaque">
                        <h2 class="h1-minha-profissao">QUERO DESCOBRIR MINHA PROFISSAO</h2>
                        <img src="img/caca-talentos-img2.png" alt="">
                        <p>Não sabe ainda qual profissão lhe interessa? Faça um "tour" entre as funções do convenia. 
                        Envie seus dados e fique atento a nossa chamada.</p>
                        <a class="bnt bnt-minha-profissao" type="button" href='QueroDescobrirMinhaProfissao.php'>Minha Profissão</a>
                    </div>
                </div>

                <div class="box box3">
                    <div class="destaque">
                        <h2 class="h1-vagas">VAGAS</h2>
                        <img src="img/caca-talentos-img3.png" alt="">
                        <p>Não sabe ainda qual profissão lhe interessa? Faça um "tour" entre as funções do convenia. 
                        Envie seus dados e fique atento a nossa chamada.</p>
                        <a class="bnt bnt-vagas" type="button" href='vagas.php'>Vagas</a>
                    </div>
                </div>

            </section>

            <div class="limpar"></div>

            <hr />

            <?php
                include("includes/footer.php")
            ?>
    </body>
</html>