<!DOCTYPE html>
<html lang="PT-BR">

<head>

	<meta charset="utf-8" />
	<title>Quero provar meu valor</title>
    <?php
        include ("includes/head.php");
    ?>
    <meta property="og:locale" content="PT-BR" />
   
    <meta property="og:title" content="Quero Provar o Meu Valor" />
    <meta property="og:site_name" content="Convenia" />

    <meta property="og:url" content="http://www.convenia.com.br/caca-talentos/" />
    <meta property="og:description" content="Não sabe ainda qual profissão lhe interessa? Faça um tour entre as funções do convenia. Envie seus dados e fique atento a nossa chamada." />

    <meta property="og:image" content="img/caca-talentos-fundo.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="8  00" />
    <meta property="og:image:height" content="440" />
    <meta property="og:type" content="website">
    
    
</head>

<body class="interna">

    <?php
        include("includes/nav.php")
    ?>

    <div class="banner">
    	<p><a href="Index.php">Home</a> > <a href="#">Meu Valor</a></p>
    	<hr />
    	<div class="ajuste-logo"></div>
    	<div id="logo">
    		<img class="logo" src="img/caca-talentos-logo-pequeno.png" alt="">
    	</div>
    	<div class="ajuste-logo"></div>
    </div>

    <section class="container-destaque">

	    <div class="form-login">
		    
            <form id="form-meu-valor" method="post">  
                <h1 class="h1-meu-valor">Quero Provar meu valor</h1>
                <?php

                    include ("php/conexao.php");
                    if($_POST){    
                        $nome = $_POST['nome'];
                        $email = $_POST['email'];
                        $mensagem = $_POST['mensagem'];
                        $data_cadastro = date('Y-m-d H:i');
                        $id_vagas = $_POST['id_vagas'];

                        $sql = "INSERT INTO tb_meu_valor (meu_valor_nome, meu_valor_email, meu_valor_mensagem, meu_valor_data_cadastro,id_vagas)  VALUES ('".$nome."','".$email."','".$mensagem."','".$data_cadastro."','".$id_vagas."')";
                        $query = mysql_query($sql);

                        if($query){

                            echo '<p class="sucesso">Enviado com sucesso!</p>';
                        }
                        else{

                            echo '  <p class="erro">Erro ao enviar</p>';

                        }
                    }

                ?>
		        <p>Não sabe ainda qual profissao lhe interessa? Faça um "tour" entre as funções do Convenia. Envie seus dados e fique atento a nossa chamada</p>
		    	<div id="form-login">
			    	<p><input type="text" id="" placeholder="Nome" required oninvalid="setCustomValidity('Por favor preencha este campo !')" name="nome"></p>
			    	<p><input type="email" id="" placeholder="E-mail" required oninvalid="setCustomValidity('Por favor preencha este campo !')" name="email"></p>
                    <p> <textarea type="text" id="" placeholder="Por que você quer provar o seu valor ?" required oninvalid="setCustomvalidity('Por Favor Preencha este campo !')" name="mensagem"></textarea></p>
                    <input type="hidden" name="id_vagas" value="<?php $id_vagas = $_GET['id_vagas']; echo $id_vagas ?>">
			    	<p><button class="bnt bnt-meu-valor" type="submit" onclick="">Enviar Dados</button></p>
			    </div>
		    </form>

	    </div>
	    <div class="imagem-destaque">
	    	<img src="img/caca-talentos2-interna.png">
	    	
	    </div>
    </section>

    <div class="limpar"></div>

    <hr />

    <?php
        include("includes/footer.php")
    ?>

    

</body>
</html>