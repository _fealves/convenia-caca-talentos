<!DOCTYPE html>
<html lang="PT-BR">

<head>

	<meta charset="utf-8" />
	<title>Vagas</title>
    <?php
        include ("includes/head.php");
    ?>
    <meta property="og:locale" content="PT-BR" />
   
    <meta property="og:title" content="Funções" />
    <meta property="og:site_name" content="Convenia" />

    <meta property="og:url" content="http://www.convenia.com.br/caca-talentos/" />
    <meta property="og:description" content="Não sabe ainda qual profissão lhe interessa? Faça um tour entre as funções do convenia. Envie seus dados e fique atento a nossa chamada." />

    <meta property="og:image" content="img/caca-talentos-fundo.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="440" />
    <meta property="og:type" content="website">
    

    
    
</head>

<body class="interna">

    <?php
        include("includes/nav.php")
    ?>

    <div class="banner">
    	<p><a href="index.php">Home</a> > <a href="">Funções</a></p>
    	<hr />
    	<div class="ajuste-logo"></div>
    	<div id="logo">
    		<img class="logo" src="img/caca-talentos-logo-pequeno.png" alt="">
    	</div>
    	<div class="ajuste-logo"></div>
    </div>

    <section class="container-conteudo">
        <div class="conteudo">
           
            <h1 class="h1-vagas">Funções</h1>
            <?php 

                include ("php/conexao.php");
                
                $sql = mysql_query("SELECT * FROM tb_vagas WHERE nome_vagas LIKE '%$palavra_chave%' OR descricao_vagas LIKE '%$palavra_chave%'");

                while ($res = mysql_fetch_array($sql)) {
                    echo '<hr />';
                    echo "<h1 class='h1-vagas'>".$res['nome_vagas']."</h1>";
                    echo "<p class='descricao'>".$res['descricao_vagas']."</p>";
                    echo "<ul class='descricao'>".$res['requisitos_vagas']."</ul>";
                
                ?>
                        <form method='get' action="meu-valor.php">
                            <input type="hidden" name="id_vagas" value="<?php echo $res['id_vagas'];?>">
                            <button class="bnt bnt-vagas" type="submit" onclick="">Candidatar</button>
                        </form>
                <?php

                
                    
                }
                     
                        
                ?>
                
            
        </div>

    </section>

    <div class="limpar"></div>

    <hr />

    <?php
        include("includes/footer.php")
    ?>

    

</body>
</html>