

<!DOCTYPE html>
<html lang="PT-BR">

<head>

	<meta charset="utf-8" />
	<title>Minha Profissão</title>
	
    <?php
        include ("includes/head.php");
    ?>
    <meta property="og:locale" content="PT-BR" />
   
    <meta property="og:title" content="Quero descobrir a minha profissão" />
    <meta property="og:site_name" content="Convenia" />

    <meta property="og:url" content="http://www.convenia.com.br/caca-talentos/" />
    <meta property="og:description" content="Não sabe ainda qual profissão lhe interessa? Faça um tour entre as funções do convenia. Envie seus dados e fique atento a nossa chamada." />

    <meta property="og:image" content="img/caca-talentos-fundo.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="440" />
    <meta property="og:type" content="website">
    
    
	
</head>

<body class="interna">

    <?php
        include("includes/nav.php");
    ?>

    <div class="banner">
    	<p><a href="Index.php">Home</a> > <a href="#">Minha Profissão</a></p>
    	<hr />
    	<div class="ajuste-logo"></div>
    	<div id="logo">
    		<img class="logo" src="img/caca-talentos-logo-pequeno.png" alt="">
    	</div>
    	<div class="ajuste-logo"></div>
    </div>

    <div class="container-destaque">
	    <div class="form-login">
		    <form id="form-minha-profissao" method="post">    
                <h1 class="h1-minha-profissao">Quero Descobrir minha profissão</h1>
                <?php

                    include ("php/conexao.php");

                    if($_POST){

                        $nome = $_POST['name'];
                        $email = $_POST['email'];
                        $mensagem = $_POST['mensagem'];
                        $data_cadastro = date('Y-m-d H:i');
                        
                        $sql = "INSERT INTO tb_minha_profissao (minha_profissao_nome, minha_profissao_email, minha_profissao_mensagem, minha_profissao_data_cadastro)  VALUES ('".$nome."','".$email."','".$mensagem."','".$data_cadastro."')";
                        $query = mysql_query($sql);

                        if($query){

                            echo '<p class="sucesso">Enviado com sucesso!</p>';
                        }
                        else{

                            echo '  <p class="erro">Erro ao enviar</p>';

                        }
                    }
                ?>
		        <p>Não sabe ainda qual profissao lhe interessa? Faça um "tour" entre as funções do Convenia. Envie seus dados e fique atento a nossa chamada</p>

		    
		    	<div id="form-login">
			    	<p><input type="text" id="" placeholder="Nome" required oninvalid="setCustomValidity('Por favor preencha este campo !')" name="name"></p>
			    	<p><input type="email" id="" placeholder="E-mail" required oninvalid="setCustomvalidity('Por favor preencha este campo !')" name="email"></p>
                    <p> <textarea type="text" id="" placeholder="Em qual area você gostaria de trabalhar ?" required oninvalid="setCustomvalidity('Por Favor Preencha este campo !')" name="mensagem"></textarea></p>
			    	<button class="bnt bnt-minha-profissao btn-form" type="submit" name="enviar">Enviar Dados</button>
                
			    </div>
                
		    </form>

	    </div>

	    <div class="imagem-destaque">
	    	<img src="img/caca-talentos-minhaProfissao.png">
	    	
	    </div>
    </div>

    <div class="limpar"></div>

    <hr />

    <?php
        include("includes/footer.php");
    ?>

    

</body>
</html>