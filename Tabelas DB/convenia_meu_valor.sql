CREATE DATABASE  IF NOT EXISTS `convenia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `convenia`;
-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: convenia
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meu_valor`
--

DROP TABLE IF EXISTS `meu_valor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meu_valor` (
  `id_meu_valor` int(11) NOT NULL AUTO_INCREMENT,
  `meu_valor_nome` varchar(100) NOT NULL,
  `meu_valor_email` varchar(100) NOT NULL,
  `meu_valor_mensagem` text,
  `meu_valor_data_cadastro` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_meu_valor`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meu_valor`
--

LOCK TABLES `meu_valor` WRITE;
/*!40000 ALTER TABLE `meu_valor` DISABLE KEYS */;
INSERT INTO `meu_valor` VALUES (1,'','','','2015-01-12 17:01'),(2,'','','','2015-01-12 17:01'),(3,'','','','2015-01-12 17:02'),(4,'Felipe Alves Teixeira','felipeloco_10@hotmail.com','teste','2015-01-12 17:02'),(5,'','','','2015-01-12 17:07'),(6,'','','','2015-01-12 17:20'),(7,'','','','2015-01-13 23:23'),(8,'','','','2015-01-13 23:24'),(9,'','','','2015-01-13 23:31'),(10,'','','','2015-01-14 10:54'),(11,'Felipe Alves Teixeira','antonio@teste.com','teste','2015-01-14 10:54'),(12,'','','','2015-01-14 12:19'),(13,'','','','2015-01-14 12:58'),(14,'','','','2015-01-14 15:27'),(15,'','','','2015-01-14 15:42'),(16,'','','','2015-01-14 15:50'),(17,'','','','2015-01-14 16:05');
/*!40000 ALTER TABLE `meu_valor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-14 16:12:44
