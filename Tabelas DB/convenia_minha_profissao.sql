CREATE DATABASE  IF NOT EXISTS `convenia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `convenia`;
-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: convenia
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `minha_profissao`
--

DROP TABLE IF EXISTS `minha_profissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `minha_profissao` (
  `id_minha_profissao` int(11) NOT NULL AUTO_INCREMENT,
  `minha_profissao_nome` varchar(100) NOT NULL,
  `minha_profissao_email` varchar(100) NOT NULL,
  `minha_profissao_mensagem` text,
  `minha_profissao_data_cadastro` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_minha_profissao`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `minha_profissao`
--

LOCK TABLES `minha_profissao` WRITE;
/*!40000 ALTER TABLE `minha_profissao` DISABLE KEYS */;
INSERT INTO `minha_profissao` VALUES (1,'Felipe','felipeloco_10@hotmail.com','teste','2015-01-12 16:25'),(2,'Felipe','felipeloco_10@hotmail.com','teste','2015-01-12 16:25'),(3,'Antonio','antonio@teste.com','teste teste','2015-01-12 16:26'),(4,'Lucia ','Lucia@teste.com','teste','2015-01-12 16:28'),(5,'Lucia ','Lucia@teste.com','teste','2015-01-12 16:32'),(6,'Robson','rob@teste.com','teste','2015-01-12 16:36'),(7,'teste','antonio@teste.com','teste','2015-01-14 15:28');
/*!40000 ALTER TABLE `minha_profissao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-14 16:12:44
